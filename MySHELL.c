#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>

void fork_error()
{
    fprintf(stderr, "Ошибка при обращении к системному вызову fork.\n");
    exit(0);
}

void memory_check (void * p)
{
    if (p==NULL)
    {
        printf("Ошибка: не хватает динамической памяти.\n");
        exit(0);
    }
}

void clean(char ** s, int n)
{
    int i;
    for (i=0; i<n; ++i) 
    {
        if (s[i]!=NULL) free(s[i]);
    }
    if (s!=NULL) free(s);
    return;
}



/*Процедура, выводящая сформированный массив лксем на экран*/

void output (char ** s, int n)
{
    int i;
    printf("Вывод лексем: \n");
    for (i=0; i<n; ++i)
    {
        printf("%s\n", s[i]);
    }
    return;
}



/*Вспомогательная функция для проверки строки*/

int err_find (char ** s, int words_num)
{
    int i, chng_put_flag=0, conv_flag=0;
    if ((strcmp(s[0], "|")==0) || (strcmp(s[0], ">")==0) || (strcmp(s[0], ">>")==0) || (strcmp(s[0], "<")==0)) return 1;
    if ((words_num>0) && ((strcmp(s[words_num-1], "|")==0) || (strcmp(s[words_num-1], ">")==0) || (strcmp(s[words_num-1], ">>")==0) || (strcmp(s[words_num-1], "<")==0))) return 1; 
    for (i=0; i<words_num-1; ++i)
    {
        if ((strcmp(s[i], "|")==0) || (strcmp(s[i], ">")==0) || (strcmp(s[i], ">>")==0) || (strcmp(s[i], "<")==0))
        {
            if ((strcmp(s[i+1], "|")==0) || (strcmp(s[i+1], ">")==0) || (strcmp(s[i+1], ">>")==0) || (strcmp(s[i+1], "<")==0)) return 1;
        }
        if ((strcmp(s[i], ">")==0) || (strcmp(s[i], ">>")==0) || (strcmp(s[i], "<")==0)) 
        {
            chng_put_flag=1;
            if (i<words_num-2)
            {
                if ((strcmp(s[i+2], ">")!=0) && (strcmp(s[i+2], ">>")!=0) && (strcmp(s[i+2], "<")!=0)) return 1;    
            }
        }
        if (strcmp(s[i], "|")==0) conv_flag=1;
        if ((strcmp(s[i], "|")==0) && (chng_put_flag==1)) return 1;
        if ((conv_flag==1) && ((strcmp(s[i], ">")==0) || (strcmp(s[i], ">>")==0) || (strcmp(s[i], "<")==0)) && (i<words_num-2))
        {
            if ((strcmp(s[i+2], ">")!=0) && (strcmp(s[i+2], ">>")!=0) && (strcmp(s[i+2], "<")!=0)) return 1;
        }
    }
    return 0;
}



/*Функция, возвращающая 0 если она не нашла ошибок в данной строке, и 1 в противном случае*/

int checking_the_correctness (char ** s, int words_num)
{
    int i, num_of_lexemes=0;
    char ** command=s, * rem_word;
    if ((strcmp(s[0], "||")==0) || (strcmp(s[0], "&")==0) || (strcmp(s[0], "&&")==0) || (strcmp(s[0], ";")==0)) return 1;
    if ((strcmp(s[words_num-1], "||")==0) || (strcmp(s[words_num-1], "&")==0) || (strcmp(s[words_num-1], "&&")==0) || (strcmp(s[words_num-1], ";")==0)) return 1;
    for (i=0; i<words_num; ++i)
    {
        if (((strcmp(s[i], "&")==0) || (strcmp(s[i], "&&")==0) || (strcmp(s[i], "||")==0) || (strcmp(s[i], ";")==0)) && (i!=words_num-1))
        {
            if ((strcmp(s[i+1], "&")==0) || (strcmp(s[i+1], "&&")==0) || (strcmp(s[i+1], "||")==0) || (strcmp(s[i+1], ";")==0)) return 1;
            else 
            {
                rem_word=s[i];
                s[i]=NULL;
                if (err_find(command, num_of_lexemes)==1) return 1;
                s[i]=rem_word;
                num_of_lexemes=0;
                command=s+i+1;
            }
        }
        else ++num_of_lexemes;
    }
    return err_find(command, num_of_lexemes);
    return 0;
}



/*Процедура, запускающая команду, не являющуюся конвейером*/

void start_not_conveyor (char ** s, int words_num, int * completion_code)
{
    int pid, i, status, d1, d2;
    char * perenapr[3];
    for (i=0; i<=2; ++i) perenapr[i]=NULL;
    for (i=0; i<words_num-1; ++i)
    {
        if ((strcmp(s[i], "<")==0) || (strcmp(s[i], ">")==0) || (strcmp(s[i], ">>")==0)) break;
    }
    if ((strcmp(s[i], "<")==0) || (strcmp(s[i], ">")==0) || (strcmp(s[i], ">>")==0))
    {
        while (s[i]!=NULL)
        {
            if (strcmp(s[i], "<")==0) perenapr[0]=s[i+1];
            else if (strcmp(s[i], ">")==0) 
            {
                perenapr[1]=s[i+1];
                perenapr[2]=NULL;
            }
            else if (strcmp(s[i], ">>")==0) 
            {
                perenapr[2]=s[i+1];
                perenapr[1]=NULL;
            }
            free(s[i]);
            s[i]=NULL;
            i=i+2;
        }
    }
    pid=fork();
    if (pid==0)
    {
        if (perenapr[0]!=NULL)
        {
            d1=open(perenapr[0], O_RDONLY, 0666);
            if (d1==-1) fprintf(stderr, "Ошибка перенаправления (ошибка при открытии файла). \n");
            else
            {
                dup2(d1, 0);
                close(d1);
            }
        }
        if ((perenapr[1]!=NULL) || (perenapr[2]!=NULL))
        {
            if (perenapr[1]!=NULL) d2=open(perenapr[1], O_TRUNC | O_WRONLY | O_CREAT, 0666);
            else d2=open(perenapr[2], O_WRONLY | O_CREAT | O_APPEND, 0666);
            if (d2==-1) fprintf(stderr, "Ошибка перенаправления (ошибка при открытии файла). \n");         
            else
            {                   
                dup2(d2, 1);
                close(d2);
            }
        }
        execvp(s[0], s);
        fprintf(stderr, "Ошибка - некорректная команда.\n");
        exit(1);
    }
    else if (pid!=-1) 
    {
        wait(&status);
        *completion_code=status;
    }
    else fork_error();
}



/*Процедура, обрабатывающая команду, не являющуюся конвейером.*/

void not_conveyor (char ** s, int words_num, int * completion_code)
{
    if (strcmp(s[0], "cd")==0)
    {
        if (s[1]!=NULL)
        {
            if (chdir(s[1])==-1) printf("Ошибка: нет директории с таким именем! \n");
        }
        else printf("Ошибка: вы не указали имя директории! \n");
    }
    start_not_conveyor(s, words_num, completion_code);
}



/*Процедура, запускающая конвейер*/

void start_conveyor (char ** s, int words_num, int conv_count, char * perenapr[3], int * completion_code) 
{
    int pr, fd[2], d1, d2, j, k, status, p;
    char ** str;
    pr=fork();
    if (pr==0)
    {
        if (perenapr[0]!=NULL) 
        {
            d1=open(perenapr[0], O_RDONLY, 0666); 
            if (d1==-1) fprintf(stderr, "Ошибка перенаправления (ошибка при открытии файла). \n");
            else
            {
                dup2(d1, 0);
                close(d1);
            }
        }
        if (perenapr[1]!=NULL) 
        {
            d2=open(perenapr[1], O_TRUNC | O_WRONLY | O_CREAT, 0666);
            if (d2==-1) fprintf(stderr, "Ошибка перенаправления (ошибка при открытии файла). \n");
            else 
            {
                dup2(d2, 1);
                close(d2);
            }
        }
        else if (perenapr[2]!=NULL) 
        {
            d2=open(perenapr[2], O_WRONLY | O_CREAT | O_APPEND, 0666);
            if (d2==-1) fprintf(stderr, "Ошибка перенаправления (ошибка при открытии файла). \n");
            else 
            {
                dup2(d2, 1);
                close(d2);
            }
        }
        for (j=0; j<words_num; ++j)
        {
            if (s[j]!=NULL)
            {
                if ((strcmp(s[j], "<")==0) || (strcmp(s[j], ">")==0) || (strcmp(s[j], ">>")==0))
                {
                    free(s[j]);
                    s[j]=NULL;
                }
            }
        }            
        j=0;
        k=0;
        while (++j<=conv_count)
        {
            pipe(fd);
            if ((p=fork())==0)
            {
                if (j!=conv_count) dup2(fd[1], 1); 
                close(fd[1]);
                close(fd[0]);
                str=s+k;
                execvp(str[0], str);
                fprintf(stderr, "Предупреждение: встретилась некорректная команда конвейера! \n");
                exit(1);
            }
            else
            {
                dup2(fd[0], 0);
                close(fd[0]);
                close(fd[1]);
                while (s[k++]!=NULL);
                wait(&status);
            }
        }
        exit(status);
    }
    else if (pr!=-1)
    {
        wait(&status);
        *completion_code=status;
        exit(0);
    }
    else fork_error();  
}



/*Процедура, определяющая есть ли конвейер в данной команде и обрабатывающая команду в случае наличия в ней конвейера.*/

void conveyor (char ** s, int words_num, int * completion_code)
{
    int conv_count=0, j, i;
    char * perenapr[3];
    for (i=0; i<3; ++i) perenapr[i]=NULL; 
    for (j=0; j<words_num; ++j)
    {
        if ((strcmp(s[j], "<")==0) || (strcmp(s[j], ">")==0) || (strcmp(s[j], ">>")==0)) 
        {
            if (strcmp(s[j], "<")==0) perenapr[0]=s[j+1];
            else if (strcmp(s[j], ">")==0) 
            {
                perenapr[1]=s[j+1];
                perenapr[2]=NULL;
            }
            else if (strcmp(s[j], ">>")==0) 
            {
                perenapr[2]=s[j+1];
                perenapr[1]=NULL;
            }
        }
        else if (strcmp(s[j], "|")==0) 
        {
            ++conv_count;
            free(s[j]);
            s[j]=NULL;
        }
    }
    ++conv_count; /*количество конвейерных процессов*/
    if (conv_count!=1) start_conveyor(s, words_num, conv_count, perenapr, completion_code);
    else not_conveyor(s, words_num, completion_code);
    return;
}



/*Функция, обрабатывающая сектор*/

void start_sector (char ** sector, int lexemes_number)
{
    int i, command_words_num=0, completion_code=0, flag=0; /*Переменная flag: 0 в случае &&, 1 в случае ||*/
    char ** command = sector;
    for (i=0; i<lexemes_number; ++i)
    {
        if (strcmp(sector[i], "&&")==0)
        {
            free(sector[i]);
            sector[i]=NULL;
            if (((flag==0) && (completion_code==0)) || ((flag==1) && (completion_code!=0))) conveyor(command, command_words_num, &completion_code);
            command=sector+i+1;
            command_words_num=0;
            flag=0;
        }
        else if (strcmp(sector[i], "||")==0)
        {
            free(sector[i]);
            sector[i]=NULL;
            if (((flag==0) && (completion_code==0)) || ((flag==1) && (completion_code!=0))) conveyor(command, command_words_num, &completion_code);
            command=sector+i+1;
            command_words_num=0;
            flag=1;
        }
        else ++command_words_num;
    }
    if (((flag==0) && (completion_code==0)) || ((flag==1) && (completion_code!=0))) conveyor(command, command_words_num, &completion_code);
}



/*Функция, выполняющая начальную обработку введенной строки*/

void execution (char ** s, int words_num)
{
    int i, lexemes_number, pid, process, status, descriptor;
    char ** sector=s;
    lexemes_number=0;
    for (i=0; i<words_num; ++i)
    {
        if (strcmp(s[i], "&")==0)
        {
            free(s[i]);
            s[i]=NULL;
            pid=fork();
            if (pid==0)
            {
                process=fork();
                if (process==0) 
                {
                    while(getppid()!=1);
                    signal(SIGINT, SIG_IGN);
                    descriptor=open("/dev/null", O_RDONLY, 0666);
                    dup2(descriptor, 0);
                    start_sector(sector, lexemes_number);
                    exit(0);
                }
                else if (process!=-1) exit(0);
                else fork_error();
            }
            else if (pid!=-1) wait(&status);
            else fork_error();
            sector=s+i+1;
            lexemes_number=0;
        }
        else if (strcmp(s[i], ";")==0)
        {
            free(s[i]);
            s[i]=NULL;
            pid=fork();
            if (pid==0)
            {
                signal(SIGINT, SIG_DFL);
                start_sector(sector,lexemes_number);
                exit(0);
            }
            else if (pid!=-1) wait(&status);
            else fork_error();
            sector=s+i+1;
            lexemes_number=0;
        }
        else ++lexemes_number;
    }
    if (s[i-1]!=NULL)
    {
        pid=fork();
        if (pid==0)
        {
            signal(SIGINT, SIG_DFL);
            start_sector(sector,lexemes_number);
            exit(0);
        }
        else if (pid!=-1) wait(&status);
        else fork_error();  
    }
}



/*Функция, считывающая и размещающая в динамической памяти очередную строку и возвращающая ссылку на нее*/

char ** input_str(char * EOF_or_enter, int * words_number)
{
    char ** s=NULL;
    char c, rem_symb;
    const int N=5;
    int m1=N, m2=N, i, flag=0, counter, getch_flag=0, words_num=0;
    s=(char **)malloc(m1*sizeof(char *));
    do
    {
        if (getch_flag==0) c=getchar();
        if (c=='"')
        {
            getch_flag=0;
            c=getchar();
            if (c!='"')
            {
                if (flag==0)
                {
                    flag=1;
                    i=0;
                    m2=N;
                    ++words_num;
                    if (words_num==m1)
                    {
                        m1+=N;
                        s=(char **)realloc(s, m1*sizeof(char *));
                        memory_check(s);
                    }
                    s[words_num-1]=(char *)malloc(m2*sizeof(char));
                    memory_check(s[words_num-1]);
                }
                while (c!='"')
                {
                    if (c=='\n')
                    {
                        printf("Ошибка: вы забыли поставить закрывающие кавычки. Повторите ввод строки.\n");
                        clean(s, words_num);
                        s=NULL;
                        words_num=0;
                        flag=0;
                        m1=N;
                        s=(char **)malloc(m1*sizeof(char *));
                        m2=N;
                        c=getchar();
                        getch_flag=1;
                        break;
                    }
                    else if (c==EOF)
                    {
                        printf("Ошибка ввода строки: вы забыли поставить закрывающие кавычки.\n");
                        clean(s, words_num);
                        exit(0);
                    }
                    s[words_num-1][i]=c;
                    ++i;
                    if (i==m2)
                    {
                        m2+=N;
                        s[words_num-1]=(char *)realloc(s[words_num-1], m2*sizeof(char));
                        memory_check(s[words_num-1]);
                    }
                    c=getchar();
                }
            }
            else 
            {
                c=getchar();
                getch_flag=1;
            }
        }
        if ((c!=' ') && (c!='\n') && (c!='(') && (c!=')') && (c!='&') && (c!='|') && (c!=';') && (c!='>') && (c!='<') && (c!=EOF) && (c!='"'))
        {
            if (flag==0) 
            {
                flag=1;
                i=0;
                m2=N;
                ++words_num;
                if (words_num==m1)
                {
                    m1+=N;
                    s=(char **)realloc(s, m1*sizeof(char *));
                    memory_check(s);
                }
                s[words_num-1]=(char *)malloc(m2*sizeof(char));
                memory_check(s[words_num-1]);
            }
            s[words_num-1][i]=c;
            ++i;
            if (i==m2)
            {
                m2+=N;
                s[words_num-1]=(char *)realloc(s[words_num-1], m2*sizeof(char));
                memory_check(s[words_num-1]);
            }
            getch_flag=0;
        }
        else if (c!='"')
        {
            getch_flag=0;
            if (flag==1)
            {
                s[words_num-1][i]='\0';
                flag=0;
            }
            if ((c=='(') || (c==')') || (c==';') || (c=='<'))
            {
                ++words_num;
                if (words_num==m1)
                {
                    m1+=N;
                    s=(char **)realloc(s, m1*sizeof(char *));
                    memory_check(s);
                }
                s[words_num-1]=(char *)malloc(2*sizeof(char));
                memory_check(s[words_num-1]);
                s[words_num-1][0]=c;
                s[words_num-1][1]='\0';
            }
            else if ((c=='&') || (c=='|') || (c=='>'))
            {
                getch_flag=1;
                counter=1;
                rem_symb=c;
                while (c==rem_symb)
                {
                    if (counter%2==0)
                    {
                        ++words_num;
                        if (words_num==m1) 
                        {
                            m1+=N;
                            s=(char **)realloc(s, m1*sizeof(char *));
                            memory_check(s);
                        }
                        s[words_num-1]=(char *)malloc(3*sizeof(char));
                        memory_check(s[words_num-1]);
                        s[words_num-1][0]=rem_symb;
                        s[words_num-1][1]=rem_symb;
                        s[words_num-1][2]='\0';
                    }
                    if ((c=getchar())==rem_symb) ++counter;
                }
                if (counter%2==1)
                {
                    ++words_num;
                    if (words_num==m1)
                    {
                        m1+=N;
                        s=(char **)realloc(s, m1*sizeof(char *));
                        memory_check(s);
                    }
                    s[words_num-1]=(char *)malloc(2*sizeof(char));
                    memory_check(s[words_num-1]);
                    s[words_num-1][0]=rem_symb;
                    s[words_num-1][1]='\0';
                }
            }
        }
    }
    while (((c!='\n') && (c!=EOF)) || (getch_flag==1));
    if (words_num>0) s[words_num]=NULL;
    *EOF_or_enter=c;
    *words_number=words_num;
    return s;
}



/*Функция main*/

int main (int argc, char * argv [])
{
    char ** s;
    char EOF_or_enter;
    int words_num=0, d, status;
    signal (SIGINT, SIG_IGN);
    if (argc==2)
    {
        d=open(argv[1], O_RDONLY);
        if (d!=-1)
        {
            dup2(d, 0);
            close(d);
        }
        else printf("Невозможно открыть файл, продолжите ввод в терминале.\n");
    }
    else if (argc>2)
    {
        printf("Ошибка: некорректное кол-во параметров командной строки.\n");
        exit(0);
    }
    printf("Пожалуйста, введите входную строку.\n");
    do
    {
        s=input_str(&EOF_or_enter, &words_num);
        /*output(s, words_num);*/
        if ((s!=NULL) && (words_num>0)) 
        {
            if (checking_the_correctness(s, words_num)==0) execution(s, words_num);
            else printf("Ошибка: некорректная входная строка!\n");
        }
        clean(s, words_num);
        if (EOF_or_enter=='\n') printf("Пожалуйста, продолжите ввод.\n");
    }
    while (EOF_or_enter=='\n');
    while (wait(&status)!=-1)
    return 0;
}
